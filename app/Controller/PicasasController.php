<?php
App::uses('Controller', 'AppController');
App::uses('CakeEmail', 'Network/Email');


/**
 * Simple Controller to list all Picasa Photos
 *
 * @package    app
 * @subpackage app.controller
 */
class PicasasController extends AppController
{
	/**
	 * Controller Components
	 *
	 * @var array
	 */
	var $components = array('Zend');


	/**
	 * Display current Users Feed
	 *
	 * @return void
	 */
	function index()
	{
		$this->Zend->loadClass('Zend_Gdata_Photos');

		$Photos = new Zend_Gdata_Photos($this->client, "Picasa-TestApplication-1.0");
		try {
			$userFeed = $Photos->getUserFeed("default");
			$this->set('userFeed', $userFeed);
		} catch (Zend_Gdata_App_HttpException $e) {
			$this->Session->setFlash('Communication Error: ' . $e->getMessage());
		} catch (Zend_Gdata_App_Exception $e) {
			$this->Session->setFlash('Application Error: ' . $e->getMessage());
		}
	}

	function authentication()

	{


		Zend_Loader::loadClass('Zend_Gdata_Photos');
		Zend_Loader::loadClass('Zend_Gdata_ClientLogin');
		Zend_Loader::loadClass('Zend_Gdata_Photos_AlbumQuery');
		Zend_Loader::loadClass('Zend_Gdata_Photos_PhotoEntry');
		Zend_Loader::loadClass('Zend_Gdata_Photos_PhotoFeed');


		$serviceName = Zend_Gdata_Photos::AUTH_SERVICE_NAME;
		$user = "sagar.parki@soarlogic.com";
		$pass = "soft123456";




		//$serviceName = Zend_Gdata_Photos::AUTH_SERVICE_NAME;
		$client = Zend_Gdata_ClientLogin::getHttpClient($user, $pass, $serviceName);

// update the second argument to be CompanyName-ProductName-Version
		$gp = new Zend_Gdata_Photos($client, "Google-DevelopersGuide-1.0");

		try {
			$userFeed = $gp->getUserFeed("default");
			foreach ($userFeed as $userEntry) {
				echo $userEntry->title->text . "<br />\n";
			}
		} catch (Zend_Gdata_App_HttpException $e) {
			echo "Error: " . $e->getMessage() . "<br />\n";
			if ($e->getResponse() != null) {
				echo "Body: <br />\n" . $e->getResponse()->getBody() .
						"<br />\n";
			}
			// In new versions of Zend Framework, you also have the option
			// to print out the request that was made.  As the request
			// includes Auth credentials, it's not advised to print out
			// this data unless doing debugging
			// echo "Request: <br />\n" . $e->getRequest() . "<br />\n";
		} catch (Zend_Gdata_App_Exception $e) {
			echo "Error: " . $e->getMessage() . "<br />\n";
		}


		// Creates a Zend_Gdata_Photos_AlbumQuery
		$query = $gp->newAlbumQuery();

		$query->setUser("default");
		$query->setAlbumName("album2");

		$albumFeed = $gp->getAlbumFeed($query);
$urls=array();
		foreach ($albumFeed as $photoEntry) {
			echo $photoEntry->title->text . "<br />\n";
		$urls[]=$photoEntry->content->src;
//			print_r($photoEntry->content->src . "<br />\n");

			
		}






		//$photoEntry = $gp->getPhotoEntry($query);

		$camera = "";
		//$contentUrl = "";
		$firstThumbnailUrl = "";

		$albumId = $photoEntry->getGphotoAlbumId()->getText();
		$photoId = $photoEntry->getGphotoId()->getText();
//print_r($photoEntry);
		if ($photoEntry->getExifTags() != null &&
				$photoEntry->getExifTags()->getMake() != null &&
				$photoEntry->getExifTags()->getModel() != null
		) {

			$camera = $photoEntry->getExifTags()->getMake()->getText() . " " .
					$photoEntry->getExifTags()->getModel()->getText();
		}

		if ($photoEntry->getMediaGroup()->getContent() != null) {
			$mediaContentArray = $photoEntry->getMediaGroup()->getContent();
	//		print_r($mediaContentArray);
			//$contentUrl = $mediaContentArray[0]->getUrl();
	//		print_r("----------------");
	//		print_r($contentUrl);

		}

		if ($photoEntry->getMediaGroup()->getThumbnail() != null) {
			$mediaThumbnailArray = $photoEntry->getMediaGroup()->getThumbnail();
			$firstThumbnailUrl = $mediaThumbnailArray[0]->getUrl();
		}

		echo "AlbumID: " . $albumId . "<br />\n";
		echo "PhotoID: " . $photoId . "<br />\n";
		echo "Camera: " . $camera . "<br />\n";

    foreach($urls as $url)
	{
		echo "Content URL ".$url . "<br />\n";
	}
$this->set('urls',$urls);

		//echo "Content URL: " . $contentUrl . "<br />\n";
		echo "First Thumbnail: " . $firstThumbnailUrl . "<br />\n";

		echo "<br />\n";


	}





}



